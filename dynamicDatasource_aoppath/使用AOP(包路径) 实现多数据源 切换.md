使用AOP 实现多数据源 切换

- 自定义动态数据源的切换方法，主要就是 写一个 AbstractRoutingDataSource 的子类啦（理论知识，自己查去）
```
package com.bkc.core;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

public class DynamicDataSource extends AbstractRoutingDataSource
{
    
    @Override
    protected Object determineCurrentLookupKey()
    {
        return DatabaseContextHolder.getCustomerType();
    }
    
}
```

```
package com.bkc.core;

public class DatabaseContextHolder
{
    
    private static final ThreadLocal<String> contextHolder = new ThreadLocal<String>();
    
    public static void setCustomerType(String customerType)
    {
        contextHolder.set(customerType);
    }
    
    public static String getCustomerType()
    {
        return contextHolder.get();
    }
    
    public static void clearCustomerType()
    {
        contextHolder.remove();
    }
}
```

- 定义一个 DataSource 的数据源配置文件

这里定义了 两个数据源，一个oracle，一个mysql。默认选取 oracle
```
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:context="http://www.springframework.org/schema/context"
	xmlns:mvc="http://www.springframework.org/schema/mvc"
	xmlns:util="http://www.springframework.org/schema/util" 
	xmlns:tx="http://www.springframework.org/schema/tx" 
	xmlns:aop="http://www.springframework.org/schema/aop" 
	xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
	http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-4.1.xsd
	http://www.springframework.org/schema/mvc http://www.springframework.org/schema/mvc/spring-mvc-4.1.xsd
	http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util-4.1.xsd
	http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-4.1.xsd 
	http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop-4.1.xsd">     
	
	<!-- 加载配置属性文件 -->
	<context:property-placeholder ignore-unresolvable="true" location="classpath:conf.properties" />
	<!-- C3P0连接池配置 -->
	<bean id="dataSourceOracle" class="com.mchange.v2.c3p0.ComboPooledDataSource">
		<property name="driverClass" value="${db1.jdbc.driver}" />
		<property name="jdbcUrl" value="${db1.jdbc.url}" />
		<property name="user" value="${db1.jdbc.username}" />
		<property name="password" value="${db1.jdbc.password}" />
		
		<!-- 配置初始化大小、最小、最大 -->
		<property name="initialPoolSize" value="${jdbc.pool.init}" />
		<property name="minPoolSize" value="${jdbc.pool.minIdle}" /> 
		<property name="maxPoolSize"  value="${jdbc.pool.maxActive}" />
		<property name="maxIdleTime" value="60000" />
	</bean>
	
	<!-- C3P0连接池配置 -->
	<bean id="dataSourceMySql" class="com.mchange.v2.c3p0.ComboPooledDataSource">
		<property name="driverClass" value="${db2.jdbc.driver}" />
		<property name="jdbcUrl" value="${db2.jdbc.url}" />
		<property name="user" value="${db2.jdbc.username}" />
		<property name="password" value="${db2.jdbc.password}" />
		
		<!-- 配置初始化大小、最小、最大 -->
		<property name="initialPoolSize" value="${jdbc.pool.init}" />
		<property name="minPoolSize" value="${jdbc.pool.minIdle}" /> 
		<property name="maxPoolSize"  value="${jdbc.pool.maxActive}" />
		<property name="maxIdleTime" value="60000" />
	</bean>
	
	<bean id="dataSource" class="com.bkc.core.DynamicDataSource">
		<property name="targetDataSources">
			<map key-type="java.lang.String">
				<entry key="dataSourceMySql" value-ref="dataSourceMySql" />
				<entry key="dataSourceOracle" value-ref="dataSourceOracle" />
			</map>
		</property>
		<property name="defaultTargetDataSource" ref="dataSourceOracle" />
	</bean>

</beans>
```
- 开启AOP 注入   
当执行 com.bkc.controller.mysql 此目录下的类的所有方法的时候，将会进入 dataSourceInterceptor的 setdataSourceMysql 方法中。    
根据aop的切入点不同来进入 设置数据源的方法中。
```
<!-- 开启aop注解方式 -->
    <aop:aspectj-autoproxy proxy-target-class="false"/>
    
<bean id="dataSourceInterceptor" class="com.bkc.core.DataSourceInterceptor" />

	<aop:config>
		<aop:aspect id="dataSourceAspect" ref="dataSourceInterceptor">
			<aop:pointcut id="dsMysql" expression="execution(* com.bkc.controller.mysql.*.*(..))" />
			<aop:pointcut id="dsOracle" expression="execution(* com.bkc.controller.oracle.*.*(..))" />
			<aop:before method="setdataSourceMysql" pointcut-ref="dsMysql"/>
			<aop:before method="setdataSourceOracle" pointcut-ref="dsOracle"/>
		</aop:aspect>
	</aop:config>
```
- 定义个 AOP 类切入点的具体方法类
```
package com.bkc.core;

import org.aspectj.lang.JoinPoint;

public class DataSourceInterceptor
{
    
    public void setdataSourceMysql(JoinPoint jp)
    {
        System.out.println("I'm Mysql");
        DatabaseContextHolder.setCustomerType("dataSourceMySql");
    }
    
    public void setdataSourceOracle(JoinPoint jp)
    {
        System.out.println("I'm Oracle");
        DatabaseContextHolder.setCustomerType("dataSourceOracle");
    }
}
```


- 执行一个正常的项目，即可看到多数据源切换的现象了