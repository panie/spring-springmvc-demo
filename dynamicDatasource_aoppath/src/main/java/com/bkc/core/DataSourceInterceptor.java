package com.bkc.core;

import org.aspectj.lang.JoinPoint;

public class DataSourceInterceptor
{
    
    public void setdataSourceMysql(JoinPoint jp)
    {
        System.out.println("I'm Mysql");
        DatabaseContextHolder.setCustomerType("dataSourceMySql");
    }
    
    public void setdataSourceOracle(JoinPoint jp)
    {
        System.out.println("I'm Oracle");
        DatabaseContextHolder.setCustomerType("dataSourceOracle");
    }
}