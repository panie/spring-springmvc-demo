package com.bkc.controller.mysql;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bkc.pojo.po.Student;
import com.bkc.service.IStudentService;

@Controller
@RequestMapping("/student/")
public class StudentController
{
    
    @Autowired
    private IStudentService studentService;
    
    @RequestMapping(value = "get")
    public String get(@RequestParam(required = false) Integer id, Model model)
    {
        if (id == null)
        {
            id = 1;
        }
        Student student = studentService.get(id);
        model.addAttribute("student", student);
        return "student";
    }
    
    @RequestMapping(value = "findList")
    @ResponseBody
    public Object findList(Model model)
    {
        List<Student> students = studentService.findList();
        return students;
    }
    
    @RequestMapping(value = "add")
    public String add(Student student, Model model)
    {
        int id = studentService.add(student);
        model.addAttribute("id", id);
        return "student";
    }
    
    @RequestMapping(value = "delete")
    public String delete(@RequestParam Integer id, Model model)
    {
        studentService.delete(id);
        model.addAttribute("delFlag", true);
        return "student";
    }
}
