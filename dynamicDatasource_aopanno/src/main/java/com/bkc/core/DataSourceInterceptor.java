package com.bkc.core;

import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

public class DataSourceInterceptor
{
    public void setdataSource(JoinPoint jp)
        throws Throwable
    {
        // logger.info("======="+jp.getSignature()+"=======");
        // 拦截的实体类
        Object target = jp.getTarget();
        
        // 拦截的方法名称
        String methodName = jp.getSignature().getName();
        // 拦截的方法参数
        Object[] args = jp.getArgs();
        
        // 拦截的放参数类型
        Class[] parameterTypes = ((MethodSignature)jp.getSignature()).getMethod().getParameterTypes();
        // 获得被拦截的方法
        Method method = target.getClass().getMethod(methodName, parameterTypes);
        if (null != method)
        {
            // 判断是否包含自定义的注解
            if (method.isAnnotationPresent(DbType.class))
            {
                // 获取自定义注解实体
                DbType dbType = method.getAnnotation(DbType.class);
                // 只针对数量较少的标了注解的方法切换数据库，其他的采用默认数据库
                if (dbType.value().equals(DbTypeEnum.DATABASE_MYSQL))
                {
                    System.out.println("I'm Mysql");
                    DatabaseContextHolder.setCustomerType(DbTypeEnum.DATABASE_MYSQL);
                    return;
                }
                
            }
            
        }
        System.out.println("I'm Oracle");
        DatabaseContextHolder.setCustomerType(DbTypeEnum.DATABASE_ORACLE);
    }
}