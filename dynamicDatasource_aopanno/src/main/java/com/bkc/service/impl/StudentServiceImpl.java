package com.bkc.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.bkc.core.DbType;
import com.bkc.core.DbTypeEnum;
import com.bkc.dao.StudentMapper;
import com.bkc.pojo.po.Student;
import com.bkc.service.IStudentService;

@Service("studentService")
public class StudentServiceImpl implements IStudentService
{
    
    @Resource
    private StudentMapper studentDao;
    
    @DbType(value = DbTypeEnum.DATABASE_MYSQL)
    public Student get(int id)
    {
        return studentDao.selectByPrimaryKey(id);
    }
    
    @DbType(value = DbTypeEnum.DATABASE_MYSQL)
    public int add(Student student)
    {
        return studentDao.insert(student);
    }
    
    @DbType(value = DbTypeEnum.DATABASE_MYSQL)
    public void delete(int id)
    {
        studentDao.deleteByPrimaryKey(id);
    }
    
    @Override
    @DbType(value = DbTypeEnum.DATABASE_ORACLE)
    public List<Student> findList()
    {
        return studentDao.findList();
    }
}
